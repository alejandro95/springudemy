package com.formationbdi.springboot.app.products.models.repository;


import com.formationbdi.springboot.app.commons.models.entity.Products;
import org.springframework.data.repository.CrudRepository;

public interface ProductsRepository extends CrudRepository<Products, Long> {
}
