package com.formationbdi.springboot.app.products.models.service;


import com.formationbdi.springboot.app.commons.models.entity.Products;

import java.util.List;

public interface IProductService {

     List<Products> findAll();

     Products findById(Long id);

     Products save(Products products);

     void deleteById(Long id);

}
