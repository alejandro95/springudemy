package com.formationbdi.springboot.app.products.controllers;


import com.formationbdi.springboot.app.commons.models.entity.Products;
import com.formationbdi.springboot.app.products.models.service.IProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController

public class ProductController {


    @Autowired
    private Environment environment;

    @Value("${server.port}")
    private Integer port;

    @Autowired
    private IProductService iProductService;


    @GetMapping("/list")
    public List<Products> list() {

        return iProductService.findAll().stream()
                .map(product -> {
                    // product.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
                    product.setPort(port);
                    return product;
                }).collect(Collectors.toList());
    }

    @GetMapping("/see/{id}")
    public Products detail(@PathVariable Long id) {

        Products products = iProductService.findById(id);
        // products.setPort(Integer.parseInt(environment.getProperty("local.server.port")));
        products.setPort(port);

        /*
        try {
            Thread.sleep(2000L);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        */

        return products;
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Products create(@RequestBody Products products){
        return iProductService.save(products);
    }

    @PutMapping("/update/{id}")
    @ResponseStatus(HttpStatus.CREATED) // asi modifiques o crees el httpstatus siempre es CREATED
    public Products update(@RequestBody Products products,@PathVariable Long id){

        Products productsDb = iProductService.findById(id);

        productsDb.setName(products.getName());
        productsDb.setPrice(products.getPrice());

        return iProductService.save(productsDb);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        iProductService.deleteById(id);
    }


}
