package com.formationbdi.springboot.app.products.models.service;

import com.formationbdi.springboot.app.commons.models.entity.Products;
import com.formationbdi.springboot.app.products.models.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ProductServiceImpl implements IProductService {

    @Autowired
    private ProductsRepository productsRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Products> findAll() {
        return (List<Products>) productsRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Products findById(Long id) {
        return productsRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public Products save(Products products) {
        return productsRepository.save(products);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {

        productsRepository.deleteById(id);
    }
}
