package com.bolsadeideas.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/params")
public class EjemploParams {

    @GetMapping("/")
    public String index() {
        return "param/index";
    }

    @GetMapping("/string")
    public String param(@RequestParam(name = "texto", defaultValue = "Jose Enmanuel", required = false) String texto, Model model) {
        //  public String param(@RequestParam(name = "texto") , Model model){
        model.addAttribute("resultado", "El texto enviado es: ".concat(texto));

        return "param/ver";
    }

    @GetMapping("/mix-params")
    public String mixparam(@RequestParam String saludo, @RequestParam Integer numero, Model model) {

        model.addAttribute("resultado", "El saludo enviado es: ".concat(saludo).concat(" y el número es ") + numero);

        return "param/ver";
    }

    @GetMapping("/mix-params-request")
    public String mixparam(HttpServletRequest request, Model model) {
        String saludo = request.getParameter("saludo");
        Integer numero = null;
        try {
            numero = Integer.parseInt(request.getParameter("numero"));
        } catch (NumberFormatException e) {
            numero = 0;
        }

        model.addAttribute("resultado", "El saludo enviado es: ".concat(saludo).concat(" y el número es ") + numero);

        return "param/ver";
    }
}
