package com.bolsadeideas.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variables")
public class EjemploVariablesRuta {

    @GetMapping("/")
    public String index(Model model) {
        model.addAttribute("titulo", "Enviar parámetros de la ruta (@PathVariable)");

        return "variables/index";
    }

    @GetMapping("/string/{texto}")
    // es una alternativa diferente --> public String variables(@PathVariable(name="") String textoOtro){
    public String variables(@PathVariable String texto, Model model) {
        model.addAttribute("titulo", "Recibir parámetros de la ruta (@PathVariable)");
        model.addAttribute("resultado", "El texto enviado en la ruta es : ".concat(texto));
        return "variables/ver";
    }

    @GetMapping("/string/{texto}/{numero}")
    public String variables(@PathVariable String texto, @PathVariable Integer numero, Model model) {
        model.addAttribute("titulo", "Recibir parámetros de la ruta (@PathVariable)");
        model.addAttribute("resultado", "El texto enviado en la ruta es : ".concat(texto).concat(" y el numero en el path es : ") + numero);
        return "variables/ver";
    }

}
