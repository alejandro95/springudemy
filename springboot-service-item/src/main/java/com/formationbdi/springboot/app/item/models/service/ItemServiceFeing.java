package com.formationbdi.springboot.app.item.models.service;

import com.formationbdi.springboot.app.item.clients.ProductClientRest;
import com.formationbdi.springboot.app.item.models.Item;
import com.formationbdi.springboot.app.commons.models.entity.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service("itemFeingService")

public class ItemServiceFeing implements ItemService {

    @Autowired
    private ProductClientRest clientFeing;

    @Override
    public List<Item> findAll() {

        return clientFeing.list().stream()
                .map(client -> new Item(client, 1))
                .collect(Collectors.toList());
    }

    @Override
    public Item findById(Long id, Integer quantity) {
        return new Item(clientFeing.detail(id), quantity);
    }

    @Override
    public Products save(Products product) {
        return clientFeing.create(product);
    }

    @Override
    public Products update(Products product, Long id) {
        return clientFeing.update(product,id);
    }

    @Override
    public void delete(Long id) {
        clientFeing.delete(id);
    }
}
