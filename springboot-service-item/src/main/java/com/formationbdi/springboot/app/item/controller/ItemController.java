package com.formationbdi.springboot.app.item.controller;


import com.formationbdi.springboot.app.item.models.Item;
import com.formationbdi.springboot.app.commons.models.entity.Products;
import com.formationbdi.springboot.app.item.models.service.ItemService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RefreshScope
@RestController
public class ItemController {

    private static Logger log = LoggerFactory.getLogger(ItemController.class);

    @Autowired
    private Environment env;


    @Autowired
    @Qualifier("ItemRestTemplateService")
    private ItemService itemService;


    @Value("${configuration.text}")
    private String text;

    @GetMapping("/list")
    public List<Item> list() {
        return itemService.findAll();
    }

    @HystrixCommand(fallbackMethod = "alternativeMethod")
    @GetMapping("/see/{id}/quantity/{quantity}")
    public Item detail(@PathVariable Long id, @PathVariable Integer quantity) {

        return itemService.findById(id, quantity);
    }

    public Item alternativeMethod(Long id, Integer quantity){

        Item  item = new Item();
        Products product = new Products();

        item.setQuantity(quantity);
        product.setId(id);
        product.setName("Camara Sony");
        product.setPrice(500.00);
        item.setProduct(product);
        return item;
    }

    @GetMapping("/obtain-config")
    public ResponseEntity<?> obtainConfig(@Value("${server.port}") String port ){

        log.info(text);
        Map<String,String> json = new HashMap<>();
        json.put("text",text);
        json.put("port",port);
        if (env.getActiveProfiles().length>0 && env.getActiveProfiles()[0].equals("dev")){

            json.put("autor.name",env.getProperty("configuration.autor.name"));
            json.put("autor.email",env.getProperty("configuration.autor.email"));
        }


        return new ResponseEntity<>(json, HttpStatus.OK);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Products create(@RequestBody Products product){

        return itemService.save(product);
    }

    @PutMapping("/update/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public Products update (@RequestBody Products product, @PathVariable Long id){
        return itemService.update(product,id);
    }

    @DeleteMapping("/delete/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id){
        itemService.delete(id);
    }

}
