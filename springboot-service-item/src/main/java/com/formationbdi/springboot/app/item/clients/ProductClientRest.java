package com.formationbdi.springboot.app.item.clients;


import com.formationbdi.springboot.app.commons.models.entity.Products;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "service-product")
public interface ProductClientRest {

    @GetMapping("/list")
     List<Products> list();

    @GetMapping("/see/{id}")
     Products detail(@PathVariable Long id);

    @PostMapping("/create")
     Products create(@RequestBody Products product);

    @PutMapping("/update/{id}")
     Products update(@RequestBody Products product, @PathVariable Long id);

    @DeleteMapping("/delete/{id}")
     void delete(@PathVariable Long id);

}
