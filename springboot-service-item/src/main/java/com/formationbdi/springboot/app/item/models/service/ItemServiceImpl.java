package com.formationbdi.springboot.app.item.models.service;

import com.formationbdi.springboot.app.item.models.Item;
import com.formationbdi.springboot.app.commons.models.entity.Products;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Primary
@Service("ItemRestTemplateService")
public class ItemServiceImpl implements ItemService {

    @Autowired
    private RestTemplate clienteRest;

    @Override
    public List<Item> findAll() {
        List<Products> product = Arrays.asList(clienteRest.getForObject("http://service-product/list", Products[].class));

        return product.stream()
                .map(p -> new Item(p, 1))
                .collect(Collectors.toList());

    }

    @Override
    public Item findById(Long id, Integer quantity) {

        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("id", id.toString());
        Products product = clienteRest.getForObject("http://service-product/see/{id}", Products.class, pathVariable);
        return new Item(product, quantity);

    }

    @Override
    public Products save(Products product) {

        HttpEntity<Products> body = new HttpEntity<Products>(product);
        ResponseEntity<Products> response =  clienteRest.exchange("http://service-product/create", HttpMethod.POST,body,Products.class);
        return response.getBody();
    }

    @Override
    public Products update(Products product, Long id) {
        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("id", id.toString());

        HttpEntity<Products> body = new HttpEntity<>(product);
        ResponseEntity<Products> response = clienteRest.exchange("http://service-product/update/{id}",HttpMethod.PUT,body, Products.class,pathVariable);

        return response.getBody();
    }

    @Override
    public void delete(Long id) {

        Map<String, String> pathVariable = new HashMap<>();
        pathVariable.put("id", id.toString());
        clienteRest.delete("http://service-product/delete/{id}",pathVariable);

    }
}
