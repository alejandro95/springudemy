package com.formationbdi.springboot.app.item.models.service;

import com.formationbdi.springboot.app.commons.models.entity.Products;
import com.formationbdi.springboot.app.item.models.Item;

import java.util.List;

public interface ItemService {

      List<Item> findAll();

      Item findById(Long id, Integer quantity);

      Products save (Products product);

      Products update(Products product, Long id);

       void delete(Long id);
}
